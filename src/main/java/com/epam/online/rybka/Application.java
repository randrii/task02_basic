/**
 * Package consists of classes that are needed to solve tasks from homework2.
 * Property of Andrii Rybka
 */
package com.epam.online.rybka;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Application class implements an application that
 * operates with odd, even numbers and Fibonacci numbers.
 *
 * @author Andrii Rybka
 * @version 1.1
 * @since 11-07-2019
 */
public class Application {

    /**
     * Variable oddNumbers stores odd numbers from the interval.
     *
     * @see List
     */
    private static List<Integer> oddNumbers = new ArrayList<Integer>();

    /**
     * Variable evenNumbers stores even numbers from the interval.
     */
    private static List<Integer> evenNumbers = new ArrayList<Integer>();

    /**
     * Variable fibonacciNumbers stores numbers of Fibonacci sequence.
     */
    private static List<Integer> fibonacciNumbers = new ArrayList<Integer>();

    /**
     * This method is used to find odd and even numbers
     * in [start, end] interval and print them on the screen.
     * Sets of numbers are static variables and
     * will use for further calculation.
     *
     * @param start This is the beginning of interval.
     * @param end   This is the end of interval.
     */
    void printNumbers(final int start, final int end) {
        // loop for searching odd numbers
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                oddNumbers.add(i);
            }
        }

        System.out.println(oddNumbers.toString());

        // loop for searching even numbers
        for (int i = end; i >= start; i--) {
            if (i % 2 == 0) {
                evenNumbers.add(i);
            }
        }

        System.out.println(evenNumbers.toString());
    }

    /**
     * This method is used to find the sums of odd and even numbers in sets
     * and print them on the screen.
     */
    void printSum() {

        // sum variables initializations
        int oddSum = 0;
        int evenSum = 0;

        // calculation of sums
        for (Integer i : oddNumbers) {
            oddSum += i;
        }

        System.out.println("Sum of odd numbers is " + oddSum);

        for (Integer i : evenNumbers) {
            evenSum += i;
        }

        System.out.println("Sum of even numbers is " + evenSum);
    }

    /**
     * This method is used to find max odd and min even numbers as f1 and f2
     * and build a Fibonacci sequence with size n, inputted by user.
     *
     * @param size Needed size for Fibonacci set
     */
    void printFibonacciNumbers(final int size) {
        // define first two elements for Fibonacci set
        int f1 = Collections.max(oddNumbers);
        int f2 = Collections.max(evenNumbers);

        // adding first two elements
        fibonacciNumbers.add(f1);
        fibonacciNumbers.add(f2);

        // finding another elements of Fibonacci set
        while (fibonacciNumbers.size() != size) {
            fibonacciNumbers.add(fibonacciNumbers.get(
                    fibonacciNumbers.size() - 1) + fibonacciNumbers.get(
                    fibonacciNumbers.size() - 2));
        }

        System.out.println(fibonacciNumbers.toString());
    }

    /**
     * This method is used to find percentage of odd and even numbers
     * and print them on the screen.
     */
    void printPercentage() {
        // define two counters for odd and even numbers
        int oddCounter = 0;
        int evenCounter = 0;
        final int percent = 100;
        double size = fibonacciNumbers.size();

        // loop into Fibonacci set
        for (int i : fibonacciNumbers) {
            if (i % 2 != 0) {
                oddCounter++;
            } else {
                evenCounter++;
            }
        }

        int oddPercentage = (int) ((oddCounter / size) * percent);
        int evenPercentage = (int) ((evenCounter / size) * percent);
        System.out.println("Percentage:\nOdd numbers\t" + oddPercentage
                + "%\nEven numbers\t" + evenPercentage + "%");
    }
}
