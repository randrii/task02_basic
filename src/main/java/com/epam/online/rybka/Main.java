package com.epam.online.rybka;

import java.util.Scanner;

/**
 * Main class is used to receive user's input
 * and call functions for calculation.
 *
 * @author Andrii Rybka
 * @version 1.0
 * @since 11-07-2019
 */
public class Main {

    /**
     * Private constructor is needed for final class.
     */
    private Main() {

    }

    /**
     * This is the main method which makes use of printNumbers, printSum,
     * printFibonacciNumber, printPercentage methods.
     *
     * @param args Unused.
     */
    public static void main(final String[] args) {

        // variable scanner used to get user's input.
        Scanner scanner = new Scanner(System.in);

        // define Application object
        Application application = new Application();

        System.out.println("Enter interval [a,b]");

        // get user's start and end values for interval
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        // call of functions
        application.printNumbers(a, b);
        application.printSum();

        System.out.print("Enter size of set: ");

        // get user's value for size of Fibonacci set
        int n = scanner.nextInt();

        application.printFibonacciNumbers(n);
        application.printPercentage();
    }
}
